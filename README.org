* System Communication Multiplexing Example
  This example demonstrates multiplexing of one UART line on an ESP32 to communicate with multiple slave devices. This frees up pins and UART hardware on the ESP to be used for other purposes.

** Motivation - why not I^{2}C or SPI?
   - Historical compatibility (e.g. adding a UART device to an existing UART communication line)
   - Third-party hardware - sometimes it is not possible to choose an ideal communication interface for outside hardware

** Comparison to I^{2}C/SPI to UART bridge
   - Cheaper than a bridge IC
   - Much simpler software interface, but also less features
